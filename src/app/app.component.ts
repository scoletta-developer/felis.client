import { Component, OnInit, Inject } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { NavigatorService } from './core/services/navigator/navigator.service';

@Component({
  selector: 'scq-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public ngOnInit(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available. Load New Version?'))
          this.navigatorService.reload();
      });
      this.swUpdate.checkForUpdate().catch((err) => {
        console.error('error when checking for update', err);
      });
    }
  }

  constructor(private swUpdate: SwUpdate, private navigatorService: NavigatorService) { }
}
