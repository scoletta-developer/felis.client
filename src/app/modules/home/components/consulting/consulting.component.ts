import { Component, OnInit } from '@angular/core';
import { NavigatorService } from '../../../../core/services/navigator/navigator.service';

@Component({
  selector: 'scq-consulting',
  templateUrl: './consulting.component.html',
  styleUrls: ['./consulting.component.scss']
})
export class ConsultingComponent implements OnInit {

  constructor(
    private navigatorService: NavigatorService
  ) { }

  ngOnInit() {
  }

  onClick(anchor: string) {

    this.navigatorService.scrollTo(anchor);
  }

}
