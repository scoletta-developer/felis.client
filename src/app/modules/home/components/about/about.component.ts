import { Component, OnInit } from '@angular/core';
import { NavigatorService } from '../../../../core/services/navigator/navigator.service';

@Component({
  selector: 'scq-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private navigatorService: NavigatorService) { }

  public options: any;

  ngOnInit() {
    this.options = {
      loop: false,
      margin: 0,
      items: 1,
      nav: false,
      autoplay: false,
      smartSpeed: 2000,
      responsiveClass: true
    };
  }

  onClick(anchor: string) {

    this.navigatorService.scrollTo(anchor);
  }
}
