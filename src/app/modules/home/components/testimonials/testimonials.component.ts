import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'scq-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {

  constructor() { }

  public options: any;

  ngOnInit() {
    this.options = {
      loop: true,
      margin: 10,
      items: 1,
      autoplay: true,
      smartSpeed: 2500,
      autoplaySpeed: false,
      responsiveClass: true,
      nav: true,
      dot: true,
      stagePadding: 0,
      navText: ['<i class="fal fa-arrow-left"></i>', '<i class="fal fa-arrow-right"></i>'],
      navContainer: '.testimonials-info'
    };
  }
}
