import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ContactModalComponent } from '../../../../shared/components/contact-modal/contact-modal.component';

@Component({
  selector: 'scq-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog() {
    this.dialog.open(ContactModalComponent);
  }

}
