import { Component, OnInit } from '@angular/core';
import { NavigatorService } from '../../../../core/services/navigator/navigator.service';

@Component({
  selector: 'scq-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

  constructor(
    private navigatorService: NavigatorService
  ) { }

  ngOnInit() {
  }

  onClick(anchor: string) {

    this.navigatorService.scrollTo(anchor);
  }
}
