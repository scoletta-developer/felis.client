import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NgwWowService } from 'ngx-wow';
import { MenuElement } from '../../../../shared/models/menu-element';

@Component({
  selector: 'scq-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss']
})
export class SkeletonComponent implements OnInit, AfterViewInit {

  constructor(private wowService: NgwWowService) { }

  public menuElements: Array<MenuElement> = [
    {
      id: 0,
      text: 'Home',
      active: true,
      target: 'intro'
    },
    {
      id: 1,
      text: 'Sviluppo',
      active: false,
      target: 'development'
    },
    {
      id: 2,
      text: 'Consulenza',
      active: false,
      target: 'consulting'
    },
    {
      id: 3,
      text: 'Chi siamo',
      active: false,
      target: 'about'
    }
    // {
    //   id: 4,
    //   text: 'Referenze',
    //   active: false,
    //   target: 'testimonials'
    // },
    // {
    //   id: 5,
    //   text: 'Partner',
    //   active: false,
    //   target: 'partner'
    // }
  ];

  ngOnInit() {

  }

  ngAfterViewInit() {

    this.wowService.init();
  }

  onChangeMenu(target: number) {

    const elements = this.menuElements;
    elements.map((menuElement) => {

      menuElement.active = false;
      if (menuElement.id === target)
        menuElement.active = true;
    });

    this.menuElements = elements;
  }

}
