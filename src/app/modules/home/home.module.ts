import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SkeletonComponent } from './pages/skeleton/skeleton.component';
import { NgwWowModule } from 'ngx-wow';

import { HomeRoutingModule } from './home-rounting.module';
import { SharedModule } from '../../shared/shared.module';
import { IntroComponent } from './components/intro/intro.component';
import { DevelopmentComponent } from './components/development/development.component';
import { ConsultingComponent } from './components/consulting/consulting.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { AboutComponent } from './components/about/about.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';

import { OwlModule } from 'ngx-owl-carousel';
import { PartnerComponent } from './components/partner/partner.component';
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';

@NgModule({
  declarations: [
    SkeletonComponent,
    IntroComponent,
    DevelopmentComponent,
    ConsultingComponent,
    ContactUsComponent,
    AboutComponent,
    TestimonialsComponent,
    PartnerComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    NgwWowModule,
    OwlModule,
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    })
  ]
})
export class HomeModule { }
