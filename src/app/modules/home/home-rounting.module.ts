import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SkeletonComponent } from './pages/skeleton/skeleton.component';

const homeRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: SkeletonComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(homeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
