import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SwUpdate, ServiceWorkerModule, UpdateAvailableEvent } from '@angular/service-worker';
import { of } from 'rxjs';
import {RouterTestingModule} from '@angular/router/testing';
import { NavigatorService } from './core/services/navigator/navigator.service';

describe('AppComponent', () => {

  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  const swUpdateMock = jasmine.createSpyObj('SwUpdate', [
    'checkForUpdate',
    'isEnabled',
    'available'
  ]);
  const navigatorMock = jasmine.createSpyObj('NavigatorService', ['reload']);
  let windowSpy: jasmine.Spy;

  beforeEach(async(() => {

    swUpdateMock.available = of<UpdateAvailableEvent>(null);
    swUpdateMock.isEnabled = true;
    swUpdateMock.checkForUpdate.and.returnValue(Promise.resolve());

    navigatorMock.reload.and.stub();

    windowSpy = spyOn(window, 'confirm');

    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        RouterTestingModule,
        ServiceWorkerModule.register('', {enabled: false})
      ],
      providers: [
        { provide: SwUpdate, useValue: swUpdateMock },
        { provide: NavigatorService, useValue: navigatorMock },
      ]
    });

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('should create the app', () => {
    expect(component).toBeTruthy();
    expect(component).toBeDefined();
  });

  it('should reload page if update is present', async () => {

    windowSpy.and.returnValue(true);

    component.ngOnInit();

    expect(await navigatorMock.reload).toHaveBeenCalled();
  });

  it('should do nothing if no update', async () => {

    windowSpy.and.returnValue(false);

    component.ngOnInit();

    expect(await navigatorMock.reload.calls.count()).toBe(1);
  });

  it('should log an error if check of update fails', async () => {

    windowSpy.and.returnValue(false);
    spyOn(console, 'error');
    swUpdateMock.checkForUpdate.and.returnValue(Promise.reject());

    component.ngOnInit();

    expect(await console.error).toHaveBeenCalled();
  });
});
