import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ButtonComponent } from './components/button/button.component';
import {MatDialogModule} from '@angular/material/dialog';
import { ContactModalComponent } from './components/contact-modal/contact-modal.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [HeaderComponent, FooterComponent, ButtonComponent, ContactModalComponent, PageNotFoundComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ButtonComponent,
    ContactModalComponent,
    MatDialogModule
  ],
  entryComponents: [
    ContactModalComponent
  ]
})
export class SharedModule { }
