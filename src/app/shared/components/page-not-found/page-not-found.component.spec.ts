import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { PageNotFoundComponent } from './page-not-found.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  template: ''
})
class DummyComponent {
}

describe('PageNotFoundComponent', () => {
  let component: PageNotFoundComponent;
  let fixture: ComponentFixture<PageNotFoundComponent>;
  // let linkDes: DebugElement[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNotFoundComponent, DummyComponent  ],
      imports: [
        RouterTestingModule.withRoutes([
          {path: '', component: DummyComponent }
        ])
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    // linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkDirectiveStub));
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a link to home', () => {

    const href = fixture.debugElement.query(By.css('a')).nativeElement.getAttribute('href');
    expect(href).toEqual('/');
  });

  it('should navigate if clicked on link', async(inject([Router, Location], (router: Router, location: Location) => {

    fixture.debugElement.query(By.css('a')).nativeElement.click();
    fixture.whenStable().then(() => {
      expect(location.path()).toEqual('/');
    });
  })));
});
