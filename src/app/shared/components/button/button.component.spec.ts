import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonComponent } from './button.component';
import { By } from '@angular/platform-browser';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let aElement: HTMLAnchorElement;

  const mockInputs = {
    link: 'test',
    anchor: 'test',
    text: 'test',
    isFull: true,
    invert: true,
    noAnimation: false
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonComponent ]
    });

    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;

    aElement = (<HTMLAnchorElement>fixture.debugElement.query(By.css('.scq-btn')).nativeElement);

    component.link = mockInputs.link;
    component.anchor = mockInputs.anchor;
    component.text = mockInputs.text;
    component.isFull = mockInputs.isFull;
    component.invert = mockInputs.invert;
    component.noAnimation = mockInputs.noAnimation;

    fixture.detectChanges();
  }));

  it('should create', () => {

    expect(component).toBeTruthy();
  });

  it('should show a text', () => {
    expect(aElement.textContent).toEqual(mockInputs.text);
  });

  it('should have a href, if link passed', () => {
    expect(aElement.getAttribute('href')).toEqual(mockInputs.link);
  });

  it('shouldn\'t have a href, if link not passed', () => {

    component.link = '';
    fixture.detectChanges();

    expect(aElement.getAttribute('href')).toBeFalsy();

    component.link = null;
    fixture.detectChanges();

    expect(aElement.getAttribute('href')).toBeFalsy();

    component.link = undefined;
    fixture.detectChanges();

    expect(aElement.getAttribute('href')).toBeFalsy();
  });

  it('should have an animation, if animation in setted', () => {

    expect(aElement.getAttribute('data-wow-delay')).toEqual('0.5s');
    expect(aElement.classList).toContain('fadeInLeft');
    expect(aElement.classList).toContain('fadeInLeft');
  });

  it('shouldn\'t have an animation, if animation is disabled', () => {

    component.noAnimation = true;
    fixture.detectChanges();

    expect(aElement.getAttribute('data-wow-delay')).toBeFalsy();
    expect(aElement.classList).not.toContain('fadeInLeft');
    expect(aElement.classList).not.toContain('wow');
  });

  it('should render correct classes', () => {

    expect(aElement.classList).toContain('scq-btn--invert');

    component.invert = false;
    fixture.detectChanges();

    expect(aElement.classList).toContain('scq-btn--full');

    component.isFull = false;
    fixture.detectChanges();

    expect(aElement.classList).toContain('scq-btn--flat');
  });

  it('should raise clicked event when clicked', async () => {

    component.clicked.subscribe((anchor: string) => {
      expect(anchor).toEqual(mockInputs.anchor);
    });

    fixture.debugElement.triggerEventHandler('click', null);
  });
});
