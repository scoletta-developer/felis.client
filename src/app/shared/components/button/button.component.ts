import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'scq-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() link: string;
  @Input() anchor: string;
  @Input() text: string;
  @Input() isFull: boolean;
  @Input() invert: boolean;
  @Input() noAnimation = false;

  @Output() clicked = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  handleClick() {

    this.clicked.emit(this.anchor);
  }
}
