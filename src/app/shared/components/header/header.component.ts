import { Component, OnInit, HostListener, Inject, Input, Output, EventEmitter } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { WINDOW } from '../../../core/services/window/window.service';
import { NavigatorService } from '../../../core/services/navigator/navigator.service';
import { MenuElement } from '../../models/menu-element';

@Component({
  selector: 'scq-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window: Window,
    private navigatorService: NavigatorService
  ) { }

  public fixed: boolean;
  public active: number;

  @Input()
  routes: Array<MenuElement>;

  @Output()
  menuVoiceSelected = new EventEmitter<number>();

  ngOnInit() {
    this.fixed = false;
    this.active = 0;
  }

  @HostListener('window:scroll', ['$event'])
  onscroll(event) {

    const offset = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
    this.fixed = offset > 0;
  }

  scrollTo(id: number, target: string) {

    this.navigatorService.scrollTo(target);
    this.menuVoiceSelected.emit(id);
  }

  goToContact() {

    this.navigatorService.scrollTo('contact');
  }
}
