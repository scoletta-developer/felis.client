import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { By } from '@angular/platform-browser';
import { WINDOW } from '../../../core/services/window/window.service';
import { DOCUMENT } from '@angular/common';
import { NavigatorService } from '../../../core/services/navigator/navigator.service';
import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({selector: 'scq-button', template: ''})
class ButtonStubComponent {
  @Input() link: string;
  @Input() anchor: string;
  @Input() text: string;
  @Input() isFull: boolean;
  @Input() invert: boolean;
  @Input() noAnimation = false;

  @Output() clicked = new EventEmitter<string>();
}

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let nativelement: HTMLElement;
  const mockInputs = [
    {
      id: 0,
      text: 'test',
      active: true,
      target: 'test'
    },
    {
      id: 1,
      text: 'second test',
      active: false,
      target: 'second test'
    }
  ];

  const navigatorServiceSpy = jasmine.createSpyObj('NavigatorService', [
    'scrollTo'
  ]);

  navigatorServiceSpy.scrollTo.and.stub();

  const windowTokenSpy = jasmine.createSpyObj('WINDOW_TOKEN', ['fake']);
  windowTokenSpy.pageYOffset = null;

  const documentTokenSpy = jasmine.createSpyObj('DOCUMENT_TOKEN', ['fake']);
  documentTokenSpy.documentElement = { scrollTop: null };
  documentTokenSpy.body = { scrollTop: null };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent,
        ButtonStubComponent
      ],
      providers: [
        { provide: NavigatorService, useValue: navigatorServiceSpy },
        { provide: WINDOW, useValue: windowTokenSpy }
      ]
    });

    TestBed.overrideComponent(
      HeaderComponent,
      {
        set: {
          providers: [
            { provide: DOCUMENT, useValue: documentTokenSpy }
          ]
        }
      }
    );

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;

    nativelement = (<HTMLElement>fixture.debugElement.query(By.css('.header_area')).nativeElement);

    component.routes = mockInputs;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize his property', () => {

    component.ngOnInit();

    expect(component.fixed).toBe(false);
    expect(component.active).toEqual(0);
  });

  it('should initialize as not sticky', () => {

    component.ngOnInit();

    expect(nativelement.classList).not.toContain('navbar_fixed');
  });

  it('should have only one selected voice of two, and should be the first', () => {

    expect(nativelement.querySelector('.nav-item').classList).toContain('active');
  });

  it('should render two voice, and render the text', () => {

    expect(nativelement.querySelectorAll('.nav-item').length).toEqual(2);
    nativelement.querySelectorAll('.nav-item').forEach((element: HTMLElement, key: number) => {

      expect(element.querySelector('.nav-link').textContent.trim()).toEqual(mockInputs[key].text);
    });
  });

  it('should raise a menu voice selected event when click on a voice, and scroll to the target', async () => {

    component.menuVoiceSelected.subscribe((id: number) => {
      expect(id).toEqual(mockInputs[1].id);
      expect(navigatorServiceSpy.scrollTo).toHaveBeenCalledTimes(1);
    });

    (<HTMLAnchorElement>nativelement.querySelectorAll('a.nav-link')[1]).click();
  });

  it('should scroll when click contact button', () => {

    component.goToContact();
    expect(navigatorServiceSpy.scrollTo).toHaveBeenCalledTimes(2);
  });

  it('should not be sticky if the page is on top', () => {

    component.fixed = true;

    TestBed.get(WINDOW).pageYOffset = null;
    fixture.debugElement.injector.get(DOCUMENT).documentElement.scrollTop = null;
    fixture.debugElement.injector.get(DOCUMENT).body.scrollTop = null;
    fixture.detectChanges();

    component.onscroll(null);

    expect(component.fixed).toBe(false);
  });

  it('should be sticky if the page is not on top (window.pageYOffset)', () => {

    component.fixed = false;

    TestBed.get(WINDOW).pageYOffset = 10;
    fixture.debugElement.injector.get(DOCUMENT).documentElement.scrollTop = null;
    fixture.debugElement.injector.get(DOCUMENT).body.scrollTop = null;
    fixture.detectChanges();

    component.onscroll(null);

    expect(component.fixed).toBe(true);
  });

  it('should be sticky if the page is not on top (document.documentElement.scrollTop)', () => {

    component.fixed = false;

    TestBed.get(WINDOW).pageYOffset = null;
    fixture.debugElement.injector.get(DOCUMENT).documentElement.scrollTop = 10;
    fixture.debugElement.injector.get(DOCUMENT).body.scrollTop = null;
    fixture.detectChanges();

    component.onscroll(null);

    expect(component.fixed).toBe(true);
  });

  it('should be sticky if the page is not on top (document.body.scrollTop)', () => {

    component.fixed = false;

    TestBed.get(WINDOW).pageYOffset = null;
    fixture.debugElement.injector.get(DOCUMENT).documentElement.scrollTop = null;
    fixture.debugElement.injector.get(DOCUMENT).body.scrollTop = 10;
    fixture.detectChanges();

    component.onscroll(null);

    expect(component.fixed).toBe(true);
  });
});
