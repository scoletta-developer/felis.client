export class MenuElement {
  public id: number;
  public text: string;
  public active: boolean;
  public target: string;
}
