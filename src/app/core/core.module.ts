import { NgModule, Optional, SkipSelf, ModuleWithProviders, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WINDOW_PROVIDERS } from './services/window/window.service';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { startUpApp } from './services/startup/startup.factory';
import { StartupService } from './services/startup/startup.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ScrollToModule.forRoot()
  ]
})
export class CoreModule {

  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule)
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
  }

  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        WINDOW_PROVIDERS,
        { provide: APP_INITIALIZER, useFactory: startUpApp, deps: [StartupService], multi: true}
       ]
    };
  }
}
