import { Injectable } from '@angular/core';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

@Injectable({
  providedIn: 'root'
})
export class NavigatorService {

  constructor(private _scrollToService: ScrollToService) { }

  public scrollTo(target: string) {

    const config = {
      target: target
    };

    this._scrollToService.scrollTo(config);
  }

  public reload() {

    location.reload();
  }
}
