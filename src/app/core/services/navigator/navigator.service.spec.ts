import { TestBed } from '@angular/core/testing';

import { NavigatorService } from './navigator.service';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';
import { Observable } from 'rxjs';

describe('NavigatoService', () => {

  let navigatorService: NavigatorService;
  let scrollToServiceSpy: jasmine.SpyObj<ScrollToService>;

  beforeEach(() => {

    const spy = jasmine.createSpyObj('ScrollToService', ['scrollTo']);

    TestBed.configureTestingModule({
      providers: [
        { provide: ScrollToService, useValue: spy }
      ]
    });

    navigatorService = TestBed.get(NavigatorService);
    scrollToServiceSpy = TestBed.get(ScrollToService);
  });

  it('should be created', () => {
    expect(navigatorService).toBeTruthy();
  });

  it('should scroll to target', () => {

    const stub = new Observable<any>();
    scrollToServiceSpy.scrollTo.and.returnValue(stub);

    const mockTarget = 'test';

    expect(() => navigatorService.scrollTo(mockTarget))
      .not.toThrowError();
    expect(scrollToServiceSpy.scrollTo.calls.count())
      .toBe(1, 'dependecy method was called once');
  });
});
