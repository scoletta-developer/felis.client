import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { SwPush } from '@angular/service-worker';

import * as LogRocket from 'logrocket';

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  constructor(public swPush: SwPush) { }

  startup(): Promise<any> {
    return Promise.all(
      [
        this.setVersion()
      ]
    );
  }

  setVersion(): Promise<any> {
    return new Promise((resolve, reject) => {

      try {

        const g = 'color:#0e4c67;font-weight:bold;';

        console.log(`
%cSCQUADRO Website
VERSION ${environment.VERSION}

For bug report please write to: info@scquadro.it

Service worker ${this.swPush.isEnabled ? 'enabled' : 'disabled'}
        `, g);

        resolve(true);
      } catch (error) {

        reject();
      }
    });
  }
}
