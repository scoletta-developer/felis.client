import { TestBed } from '@angular/core/testing';

import { StartupService } from './startup.service';
import { SwPush } from '@angular/service-worker';
import { doesNotThrow } from 'assert';

describe('StartupService', () => {

  let startupService: StartupService;
  let swPushSpy: jasmine.SpyObj<SwPush>;

  beforeEach(() => {

    const spy = jasmine.createSpyObj('SwPush', ['requestSubscription']);
    spy.isEnabled = true;

    TestBed.configureTestingModule({
      providers: [
        { provide: SwPush, useValue: spy }
      ]
    });

    startupService = TestBed.get(StartupService);
    swPushSpy = TestBed.get(SwPush);
  });

  it('should be created', () => {
    expect(startupService).toBeTruthy();
  });

  it('should resolve a promise with true', (done) => {

    spyOn(console, 'log').and.stub();

    expect(() => startupService.startup())
      .not.toThrowError();

    startupService.startup()
    .then((result) => {
      expect(result).toEqual([true]);
      done();
    })
    .catch(() => fail('Exception in function'));
  });
});
