#!/usr/bin/env bash

#Get destination branches
DEST_BRANCHES=("$@")

for ((i=0; i < $#; i++))
{
  DEST_BRANCH="${DEST_BRANCHES[$i]}"

  #Interpret the input
  if [ "${DEST_BRANCHES[$i]}" = "release" ]; then
    RESPONSE_RELEASE=`curl -s "https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/refs/branches?q=name~\"release\"" \
    --silent \
    --write-out "HTTPSTATUS:%{http_code}" \
    --user $BB_USER:$BB_APP_PASSWORD`

    RELEASE_HTTP_STATUS=$(echo $RESPONSE_RELEASE | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

    if [ ! $RELEASE_HTTP_STATUS -eq 200 ]; then
      printf "\e[31m\nBranch [$DEST_BRANCH]\nError calling API to get the last release.\nError Code: [$RELEASE_HTTP_STATUS]\e[0m"

      RELEASE_ERROR=$(echo $RESPONSE_RELEASE | sed -e 's/HTTPSTATUS\:.*//g')
      if [ ! $RELEASE_HTTP_STATUS -eq 403 ]; then
        RELEASE_ERROR=$(echo $RESPONSE_RELEASE | sed -e 's/HTTPSTATUS\:.*//g' | jq '.error.message')
      fi

      if [ $RELEASE_HTTP_STATUS -eq 404 ]; then
        continue
      fi      
      
      printf "\e[31m\nError message: $RELEASE_ERROR\n\e[0m"

      exit 1
    fi

    RELEASE_BODY=$(echo $RESPONSE_RELEASE | sed -e 's/HTTPSTATUS\:.*//g')
    DEST_BRANCH=$(echo $RELEASE_BODY | jq -r '.values[0].name')
  fi

  if [ "$DEST_BRANCH" = "null" ]; then
    printf "\e[31mThe requested branch is missing\n\e[0m"
    continue
  fi
  #Create new PR and get its ID
  printf "Creating PR: $BITBUCKET_BRANCH -> $DEST_BRANCH\n"

  DATA='{
    "title": "Automatic PR '$BITBUCKET_BRANCH' -> '$DEST_BRANCH'",
    "description": "Automatic PR for build #'$BITBUCKET_BUILD_NUMBER' on branch '$BITBUCKET_BRANCH'",
    "state": "OPEN",
    "destination": {
      "branch": {
        "name": "'$DEST_BRANCH'"
        }
    },
    "source": {
      "branch": {
        "name": "'$BITBUCKET_BRANCH'"
      }
    }
  }'

  RESPONSE_CREATE=`curl -X POST https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/pullrequests \
  --silent \
  --write-out "HTTPSTATUS:%{http_code}" \
  --user $BB_USER:$BB_APP_PASSWORD \
  -H 'content-type: application/json' \
  -d "$DATA"`

  CREATE_HTTP_STATUS=$(echo $RESPONSE_CREATE | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

  if [ ! $CREATE_HTTP_STATUS -eq 201 ]; then
    printf "\e[31m\nBranch [$DEST_BRANCH]\nError calling API to create the pull request.\nError Code: [$CREATE_HTTP_STATUS]\e[0m"

    CREATE_ERROR=$(echo $RESPONSE_CREATE | sed -e 's/HTTPSTATUS\:.*//g')
    if [ ! $CREATE_HTTP_STATUS -eq 403 ]; then
      CREATE_ERROR=$(echo $RESPONSE_CREATE | sed -e 's/HTTPSTATUS\:.*//g' | jq '.error.message')
    fi
    printf "\e[31m\nError message: $CREATE_ERROR\n\e[0m"

    exit 1
  fi

  CREATE_BODY=$(echo $RESPONSE_CREATE | sed -e 's/HTTPSTATUS\:.*//g')
  PR_ID=$(echo $CREATE_BODY | jq -r '.id')

  printf "\e[32mPull request CREATED\n\n\e[0m"

  #Merge PR
  printf "Merging PR $PR_ID\n"

  RESPONSE_MERGE=`curl -X POST https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/pullrequests/$PR_ID/merge \
  --silent \
  --write-out "HTTPSTATUS:%{http_code}" \
  --user $BB_USER:$BB_APP_PASSWORD`
  
  MERGE_HTTP_STATUS=$(echo $RESPONSE_MERGE | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

  if [ ! $MERGE_HTTP_STATUS -eq 200 ]; then
    printf "\e[31m\nBranch [$DEST_BRANCH]\nError merging the pull request.\n\nError Code: [$MERGE_HTTP_STATUS]\e[0m"

    MERGE_ERROR=$(echo $RESPONSE_MERGE | sed -e 's/HTTPSTATUS\:.*//g' | jq '.error.message')
    printf "\e[31m\nError message: $MERGE_ERROR\n\e[0m"

    exit 1
  fi
  
  printf "\e[32mPR $PR_ID MERGED\n\e[0m"
}
