# Codename PROJECT FELIS

> Company website for SCQUADRO Software Agency

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The project needs the below prerequisites to run locally

* node > 11.1.0
* angular-cli installed globally

### Installing

Clone the repository from this url [https://simonecoletta@bitbucket.org/scquadro/felis.client.git](https://simonecoletta@bitbucket.org/scquadro/felis.client.git)

```bash
git clone https://simonecoletta@bitbucket.org/scquadro/felis.client.git
```

Install the npm dependencies

```bash
npm install
```

## Development environment

In development environment you can use angular cli for serve and build the application

### Serve

```bash
ng serve -o
```

### Build (production ready code)

```bash
ng build --prod
```

For additional information about angular cli commands, please follow the official documentation: [angular cli documentation](https://github.com/angular/angular-cli/wiki)

## Deployment

The project is ready to be deployed on firebase.

First of all install firebase tools

```bash
npm install -g firebase-tools
```

then init it

```bash
firebase init
```

and finally deploy the builded code (located in dist/browser folder)

```bash
firebase deploy
```

## Prerender

The website could be prerendered at build time to increase the performance using Angular Universal (additional information: [Angular Universal](https://angular.io/guide/universal))

### Serve prerender

To serve locally the prerendered code install http-server and launch the following command

```bash
npm run build:serve:prerender
```

### Build prerender

To build the code to be ready to be deployed, instead:

```bash
npm run build:optimized
```

## Built With

* [Angular](https://angular.io/) - Web framework
* [Angular Universal](https://angular.io/guide/universal) - Prerendering and server-side rendering
* [npm](https://www.npmjs.com/) - Package Management

## Authors

* **Simone Coletta** - [SimoneColetta](https://github.com/collets)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Release notes

### 1.6.0

* Add 404 page (issue [13](https://bitbucket.org/scquadro/felis.client/issues/13/))

### 1.5.0

* General improvements

### 1.4.2

* Minor bugfix
* Remove LogRocket (issue [20](https://bitbucket.org/scquadro/felis.client/issues/20/))

### 1.4.1

* Minor bugfix (issue [19](https://bitbucket.org/scquadro/felis.client/issues/19/))

### 1.4.0

* Add LogRocket (issue [4](https://bitbucket.org/scquadro/felis.client/issues/4/))
* Improve environments in build scripts (issue [16](https://bitbucket.org/scquadro/felis.client/issues/16/))
* Improve logging for tests and added tests results (issue [14](https://bitbucket.org/scquadro/felis.client/issues/14/))

### 1.3.0

* Unit tests init (issue [#1](https://bitbucket.org/scquadro/felis.client/issues/1/))
* Improved cache for firebase

### 1.2.0

* Update Readme.md
* Update color of mobile browser bar (issue [#2](https://bitbucket.org/scquadro/felis.client/issues/2/))

### 1.1.1

* BUGFIX: Fixed images on Safari (issue [#3](https://bitbucket.org/scquadro/felis.client/issues/3/))

### 1.1.0

* Remove unused menu voice
* Update configuration for CI

### 1.0.1

* Update firebase deploy commands

### 1.0.0

* Website up and running
* Prerendering at build time
* Service worker enabled
* Progressive Web App functionalities enabled